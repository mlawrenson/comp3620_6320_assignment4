# COMP3620/6320 Artificial Intelligence
# The Australian National University - 2015
# COMP3620 and COMP6320 - Assignment 4

""" Student Details
    Student Name: 
    Student number:
    Date:
"""

""" This class contains placeholders classes in which you will implement
    your agents that interact with the various grid worlds.
    
    These agents will extend RLAgent and must implement the select_action and
    update_Q methods.
    
    You should comment your implementations and submit this file.
"""

import random, abc


class RLAgent(object):
    """ Generic class for RLAgent """
    
    def __init__(self, alpha, gamma, epsilon, num_states, actions):
        """ Make a new RLAgent with the specified parameters.
            (RLAgent, float, float, float, int, int) -> None
        """
        self.alpha = alpha
        self.gamma = gamma
        self.epsilon = epsilon    
        self.num_states = num_states
        self.actions = actions  # Actually a list of strings
        #Q is a dictionary mapping (state, action) pairs to Q values
        self.Q = dict([((s, a), 0.0) for s in xrange(num_states) for a in actions])
    
    @abc.abstractmethod
    def select_action(self, state):
        """ This returns an action based on the value of the Q-table,
            and the exploration constant (epsilon). Break ties uniformly at random.
            
            (RLAgent, int) -> str
        """
        
    @abc.abstractmethod
    def update_Q(self, state_action, reward, new_state, new_action):
        """  Update the Q-value table. Decide what arguments to pass into it.
            (RLAgent, ??) -> None
        """

###############################################################################
#                         Write your code below                               #
###############################################################################

###############   Implement the following two classes #########################


class Sarsa(RLAgent):
    """ The Sarsa agent implements the Sarsa algorithm.
        You must override select_action and update_Q.
    """
    def select_action(self, state):
        """ This returns an action based on the value of the Q-table,
            and the exploration constant (epsilon). Break ties uniformly at random.
            (RLAgent, int) -> str
        """
        # Generate random number on [0,1] with uniform distribution
        # and check if it is < epsilon
        if random.uniform(0, 1) < self.epsilon:
            # Choose to take a random action with probability epsilon
            # (rand_num is uniform, so P(rn<e)=e)
            return random.choice(self.actions)
        else:
            # Choose to take the greedy option with probability 1 - epsilon
            return self.greedy_action(state)[0]

    def greedy_action(self, state):
        """
        Just returns the greedy (state, action) at state
        :param state: int
        :return: (int, str)
        """
        # Set ininital values to the first element
        argmax = self.actions[0]
        max_q = self.Q[(state, argmax)]

        # Loop over remaining actions
        num_tie = 0
        for action in self.actions[1:]:
            # Get Q for this action
            q = self.Q[(state, action)]
            if q > max_q:
                # If this action has a higher Q than max, replace max
                argmax, max_q = action, q
            elif q == max_q:
                # If Q is equal, increment number of ties
                num_tie += 1
                # 1/num_tie chance this action replaces argmax
                if random.randrange(num_tie) == num_tie:
                    argmax = action

        # Return argmax and maximum Q
        return argmax, max_q

    def update_Q(self, state_action, reward, new_state, new_action):
        """  Update the Q-value table. Decide what arguments to pass into it.
        """
        # Update Q(s,a) using (s',a') and reward
        self.Q[state_action] += self.alpha * (reward + self.gamma *
                                              self.Q[(new_state, new_action)]
                                              - self.Q[state_action])

class QLearn(Sarsa):
    """ The QLEarn agent implements the Q-learning algorithm.
        You must override select_action and update_Q.
    """
    def update_Q(self, state_action, reward, new_state, new_action=None):
        """  Update the Q-value table. Decide what arguments to pass into it.
        """
        # Update Q(s,a) using s' and reward
        self.Q[state_action] += self.alpha * (reward + self.gamma *
                                              self.greedy_action(new_state)[1]
                                              - self.Q[state_action])
