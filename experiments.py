# COMP3620/6320 Artificial Intelligence
# The Australian National University - 2015
# COMP3620 and COMP6320 - Assignment 4

""" Student Details
    Student Name: 
    Student number:
    Date:
"""

""" In this file you should write code to generate the graphs that
    you include in your report in answers.pdf.
    
    We have provided the skeletons of functions which you can use to
    run Sarsa and Q-learning if you choose.
    
    We suggest using Matplotlib to make plots. Example code to make a
    plot is included below. Matplotlib is installed on the lab computers.
    On Ubuntu and other Debian based distributions of linux, you can install it
    with "sudo apt-get install python-matplotlib".
    
    Please put the code to generate different plotsinto different functions and
    at the bottom of this file put clearly labelled calls to these
    functions, to make it easier to test your generation code.
"""

# Uncomment this if you are running on a VM and don't want to use X11 forwarding
# Plots should still be saved fine.
import matplotlib
matplotlib.use('agg')

from environment import WindyGrid, WindyGridWater
from agents import Sarsa, QLearn
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import itertools
import multiprocessing

def get_windy_grid():
    """ This function simplifies making a WindyGrid environment.
        You should call this function to get the environment with the desired
        wind probability (delta) and water penalty.
        
        For Q3 you should call this function to get the environment for Q2.
        
        (float, float) -> GridWater
    """
    return WindyGrid(10, 7, (0, 3), (7, 3), [0, 0, 0, 1, 1, 1, 2, 2, 1, 0])

def get_windy_grid_water(delta, water_reward):
    """ This function simplifies making a WindyGridWater environment.
        You should call this function to get the environment with the desired
        wind probability (delta) and water penalty.
        
        For Q3 you should call this function with water_reward=-10 and varying delta.
        
        For Q4 and Q5 you should call this function with delta=0 and water_reward=-100.
        
        (float, float) -> WindyGridWater
    """
    return WindyGridWater(10, 7, (0, 1), (7, 1),
        [(3, 0), (4, 0), (3, 2), (4, 2), (3, 3), (4, 3)], delta, water_reward)


###############################################################################
#                         Write your code below                               #
###############################################################################

def main():
    # Note that the datasets produced are large
    #  It takes some time to run these. To run much faster, just reduce n_runs to 2 or 3.
    q2_save_fig('tex/figures/q2_fig1.pdf', [0, 0.2, 0.4, 0.6, 0.8, 1], 15000, 100)
    q2_save_fig('tex/figures/q2_fig2.pdf', [0.5, 0.55, 0.6, 0.65, 0.7], 25000, 100)
    q2_save_fig('tex/figures/q2_fig3.pdf', [0.1, 0.2, 0.3, 0.4, 0.5], 50000, 100)
    q3i_save_fig('tex/figures/q3_fig1.pdf', np.linspace(0,1,11), 75000, 50)
    q3ii_save_fig('tex/figures/q3_fig2.pdf', np.linspace(0, 1, 201), 200)
    q4_save_fig('tex/figures/q4_fig1.pdf', np.linspace(0.1, 0.7, 49), 200)
    q4_reward_save_fig('tex/figures/q4_fig1_reward.pdf', np.linspace(0.1, 0.7, 49), 100)
    q4i_save_fig('tex/figures/q4_fig2.pdf', np.linspace(0.1, 0.7, 7), 20000, 100)
    q4_save_fig('tex/figures/q4_fig1_e0.pdf', np.linspace(0, 0.1, 11), 100)
    find_lowest_q('tex/figures/q5.txt', 1e9, -0.5)

def find_lowest_q(out_fn, time_lim, q_step):
    """
    This function will find the lowest Q initialisation that completes one episode in time_lim
    With Sarsa on Q4 Environment
    :param out_fn: Filename to write results to
    :param q_step: Step size for Q
    :return: None
    """
    # Set up environment and agent
    env = get_windy_grid_water(0, -100)
    agent = Sarsa(0.1, 0.99, 0, env.num_states, env.actions)

    # Open output file
    with open(out_fn, 'w') as out_f:
        out_f.write('Finding lowest initial Q for Sarsa (Q5)\n')
        out_f.write('Time Limit of: %d\n' % time_lim)
        out_f.write('Stepping Q by %d\n' % q_step)

        # Run loop while ep_len < time_lim
        ep_len = 0
        curr_q = 0.0
        while ep_len < time_lim:
            # Increment Q
            curr_q += q_step

            # Re initialise Q with curr_q
            agent.Q = dict([((s, a), curr_q) for s in xrange(env.num_states) for a in env.actions])

            # Get episode length
            ep_len = run_ep(env, agent, time_lim)
            out_f.write('Q=%f has episode length: %d\n' % (curr_q, ep_len))

        # Write result
        out_f.write('------------------------------------\n\n')
        out_f.write('Time limit hit with Q=%f\n' % curr_q)

def run_ep(env, agent, max_time=20000, reward=False):
    """
    This function will run an episode of agent on env
    :param env: Environment
    :param agent: Agent (Sarsa or QLearn
    :param max_time: int (maximum time allowed for an episode)
    :return: int
    """
    if isinstance(agent, Sarsa):
        return run_ep_sarsa(env, agent, max_time, reward)
    elif isinstance(agent, QLearn):
        return run_ep_qlearn(env, agent, max_time, reward)
    else:
        raise TypeError('Agent not Sarsa or QLearn')

def run_ep_sarsa(env, agent, max_time=20000, reward=False):
    """
    This function will run an episode of agent on env
    :param env: Environment
    :param agent: Sarsa
    :param max_time: int (maximum time allowed for an episode)
    :return: int
    """
    # Get starting state
    state = env.pos_to_state(env.current_pos)

    # Get first action
    action = agent.select_action(state)

    # Set up counter
    ctr = 0
    cum_reward = 0.0

    # Loop until the end of the episode or maximum time reached
    while not env.end_of_episode():
        # Take action and observe the new state and reward
        new_state, reward = env.generate(action)
        cum_reward += reward

        # Get next action
        new_action = agent.select_action(new_state)

        # Update agent's Q
        agent.update_Q((state, action), reward, new_state, new_action)

        # Update state and action for next iteration
        state, action = new_state, new_action

        # Increment counter
        ctr += 1

        # Check if it has reached the step limit
        if ctr == max_time:
            # Reset environment
            env.current_pos = env.init_pos
            env.init_state = env.pos_to_state(env.init_pos)

            # Just return the step limit to keep data consistent
            if reward:
                return cum_reward
            else:
                return max_time

    # Return the number of steps
    # agent.Q will be modified
    if reward:
        return cum_reward
    else:
        return ctr

def run_ep_qlearn(env, agent, max_time=20000, reward=False):
    """
    This function will run an episode of agent on env
    :param env: Environment
    :param agent: QLearn
    :param max_time: int (maximum time allowed for an episode)
    :return: int
    """
    # Get starting state
    state = env.pos_to_state(env.current_pos)

    # Set up counter
    ctr = 0
    cum_reward = 0.0

    # Loop until the end of the episode
    while not env.end_of_episode() and ctr < max_time:
        # Get action
        action = agent.select_action(state)

        # Take action and observe the new state and reward
        new_state, reward = env.generate(action)
        cum_reward += reward

        # Update agent's Q
        agent.update_Q((state, action), reward, new_state)

        # Update state and action for next iteration
        state = new_state

        # Increment counter
        ctr += 1

        # Check if it has reached the step limit
        if ctr == max_time:
            # Reset environment
            env.current_pos = env.init_pos
            env.init_state = env.pos_to_state(env.init_pos)

            # Just return the step limit to keep data consistent
            if reward:
                return cum_reward
            else:
                return max_time

    # Return the number of steps
    # agent.Q will be modified
    if reward:
        return cum_reward
    else:
        return ctr

def q2_get_result_eps(alpha, max_time):
    """
    Function that returns the number of episodes completed by Sarsa before max_time
    :param alpha: float - Alpha for Sarsa
    :param max_time: int - Maximum number of timesteps for each alpha
    :return: [int] - Cumulative timesteps where episodes finished
    """
    # Set up environment and agent
    env = get_windy_grid()
    agent = Sarsa(alpha, 0.99, 0.01, env.num_states, env.actions)

    # Loop until max_time has been reached
    steps = [0]
    while steps[-1] < max_time:
        # Append trace for episode
        steps.append(steps[-1] + run_ep(env, agent, max_time))

    # Get rid of last data point if past max_time
    if steps[-1] > max_time:
        steps.pop()

    # Return the cumulative timesteps where episodes finished
    return steps

def q2_inner_func((max_time, alpha, run)):
    """ Just the inner function for the pool """
    steps = q2_get_result_eps(alpha, max_time)

    # Return the column of the DataFrame with episode number as result
    return pd.DataFrame(range(len(steps)), index=steps,
                        columns=pd.MultiIndex.from_tuples([(alpha, run)], names=['alpha', 'run']))

def q2_save_fig(out_fn, alphas, max_time, n_runs):
    """
    Gets results for running Sarsa on various alpha and saves plot
    :param out_fn: str - Filename to save figure to
    :param alphas: [float] - List of values to use for alpha
    :param max_time: int - Maximum number of timesteps for each alpha
    :param n_runs: int - number of runs per alpha
    :return: None
    """
    # Get results in parallel
    pl = multiprocessing.Pool(multiprocessing.cpu_count())
    dfs = pl.map(q2_inner_func, itertools.product([max_time], alphas, xrange(n_runs)))
    pl.close()

    # Concatenate results sideways
    df = pd.concat(dfs, axis=1)

    # Interpolate missing values so that there is meaningful data for all timesteps
    df.interpolate(method='values', inplace=True)

    # Save data
    df.to_pickle(out_fn.split('.')[0] + '.dat')

    # Get data into long-form for plotting
    ldf = df.unstack().reset_index()
    ldf.columns = ['alpha', 'run', 'time', 'episode']

    # Plot this with seaborn
    sns.tsplot(ldf, time='time', unit='run', condition='alpha', value='episode',
               err_style="ci_band", ci=99, n_boot=1500)
    plt.savefig(out_fn, dpi=600, bbox_inches='tight')
    plt.close()

    # Plot unit traces
    sns.tsplot(ldf, time='time', unit='run', condition='alpha', value='episode',
               err_style="unit_traces", n_boot=0)
    plt.savefig(out_fn.split('.')[0] + '_unit.pdf', dpi=600, bbox_inches='tight')

def q3i_get_result_eps(delta, max_time):
    """
    Function that returns the number of episodes completed by Sarsa at times in times
    :param delta: float - Delta for environment in this run
    :param max_time: int - Maximum number of timesteps for each alpha
    :return: [int] - Cumulative timesteps where episodes finished
    """
    # Set up environment and agent
    env = get_windy_grid_water(delta, -10)
    agent = Sarsa(0.1, 0.99, 0.005, env.num_states, env.actions)

    # Loop until max_time has been reached
    steps = [0]
    while steps[-1] < max_time:
        # Append trace for episode
        steps.append(steps[-1] + run_ep(env, agent, max_time))

    # Get rid of last data point if past max_time
    if steps[-1] > max_time:
        steps.pop()

    # Return the cumulative timesteps where episodes finished
    return steps

def q3i_inner_func((max_time, delta, run)):
    """ Just the inner function for the pool """
    steps = q3i_get_result_eps(delta, max_time)

    # Return the column of the DataFrame with episode number as result
    return pd.DataFrame(range(len(steps)), index=steps,
                        columns=pd.MultiIndex.from_tuples([(delta, run)], names=['delta', 'run']))

def q3i_save_fig(out_fn, deltas, max_time, n_runs):
    """
    Gets results for running Sarsa on various delta and saves plot
    :param out_fn: str - Filename to save figure to
    :param deltas: [float] - List of values to use for delta
    :param max_time: int - Maximum number of timesteps for each alpha
    :param n_runs: int - number of runs per alpha
    :return: None
    """
    # Get results in parallel
    pl = multiprocessing.Pool(multiprocessing.cpu_count())
    dfs = pl.map(q3i_inner_func, itertools.product([max_time], deltas, xrange(n_runs)))
    pl.close()

    # Concatenate results sideways
    df = pd.concat(dfs, axis=1)

    # Interpolate missing values so that there is meaningful data for all timesteps
    df.interpolate(method='values', inplace=True)

    # Save data
    df.to_pickle(out_fn.split('.')[0] + '.dat')

    # Get data into long-form for plotting
    ldf = df.unstack().reset_index()
    ldf.columns = ['delta', 'run', 'time', 'episode']

    # Plot this with seaborn
    sns.tsplot(ldf, time='time', unit='run', condition='delta', value='episode',
               err_style="ci_band", ci=99, n_boot=1500)
    plt.savefig(out_fn, dpi=600, bbox_inches='tight')
    plt.close()

    # Plot unit traces
    sns.tsplot(ldf, time='time', unit='run', condition='delta', value='episode',
               err_style="unit_traces", n_boot=0)
    plt.savefig(out_fn.split('.')[0] + '_unit.pdf', dpi=600, bbox_inches='tight')

def converge(env, agent, ep_limit, tol):
    """
    Function that takes agent to convergence on environment
    This looks at the average episode length for a 100 episode window and compares it to the
    last 100 episode window, with a 50 episode gap in between.
    The agent is considered to have converged when the difference between
    these windows is less than the tolerance or the episode limit is hit
    :param env: Environment
    :param agent: Agent
    :param ep_limit: int - limit of episodes while looking for convergence - don't set this too low
    :param tol: float - tolerance for convergence
    :return: int - if 0 then no convergence, >0 is episodes required to converge
    """
    # Parameters
    window_size = 100
    gap_size = 50

    # Initialise first window
    last_avg = 1.0 * sum(run_ep(env, agent, 50000) for _ in xrange(window_size)) / window_size

    # Run over the first gap
    for _ in xrange(gap_size):
        run_ep(env, agent, 50000)

    # Get the second window and difference
    curr_avg = 1.0 * sum(run_ep(env, agent, 50000) for _ in xrange(window_size)) / window_size
    curr_diff = abs(curr_avg - last_avg)

    # Loop until tolerance is reached or episode limit is hit
    i = 2 * window_size + gap_size
    while curr_diff > tol:
        # Check if episode limit is going to be hit this iteration
        if i + gap_size + window_size >= ep_limit:
            print('Did not reach tolerance after %d episodes' % ep_limit)
            print('Current difference: %f' % curr_diff)
            return 0

        # Set last_avg
        last_avg = curr_avg

        # Run a gap
        for _ in xrange(gap_size):
            run_ep(env, agent, 50000)

        # Get window average and diff
        curr_avg = 1.0 * sum(run_ep(env, agent, 50000) for _ in xrange(window_size)) / window_size
        curr_diff = abs(curr_avg - last_avg)

        # Increment i by
        i += gap_size + window_size

    return i

def converge_Q(env, agent, ep_limit, tol):
    """
    Function that takes agent to convergence on environment
    I am calling convergence the point where Q has reached is invariant
    This is not used as looking at the mean distance of Q is very slow.
    :param env: Environment
    :param agent: Agent
    :param ep_limit: int - limit of episodes while looking for convergence
    :param tol: float - tolerance for convergence
    :return: int - if 0 then no convergence, >0 is episodes required to converge
    """
    num_diff = 50
    # If episole limit is lower than 10, return false already
    if ep_limit <= num_diff:
        return 0

    # Initialise array of last Q values
    last_qs = np.array([agent.Q.values()] * num_diff)

    # Run the first nine episodes to get the initial array
    for i in xrange(1, num_diff):
        run_ep(env, agent)
        last_qs[i % num_diff] = agent.Q.values()

    # Get distance of these
    dist = np.mean([sum(abs(x[i] - y[i])
                        for i in xrange(len(x)))
                    for x, y in itertools.combinations(last_qs, 2)])

    # Loop until the mean pairwise 1-distance is less than a tolerance
    i = num_diff
    while dist > tol:
        run_ep(env, agent)
        last_qs[i % num_diff] = agent.Q.values()
        i += 1
        if i == ep_limit:
            print('Did not reach tolerance after %d episodes' % ep_limit)
            print('Current distance: %f' % dist)
            return 0
        elif i % num_diff == 0:
            # Only compute this every 10 iterations.
            dist = np.mean([sum(abs(x[i] - y[i])
                                for i in xrange(len(x)))
                            for x, y in itertools.combinations(last_qs, 2)])

    return i

def q3ii_ep_len(delta, max_episodes):
    """
    Function that finds the average final episode length for a delta as in Q3
    :param delta: float - wind probability
    :param max_episodes: int - maximum time to convergence
    :return: average final episode length
    """
    print('Getting result with delta=%.2f' % delta)
    # Set up environment and agent
    env = get_windy_grid_water(delta, -10)
    agent = Sarsa(0.1, 0.99, 0.005, env.num_states, env.actions)

    # Converge agent
    if converge(env, agent, max_episodes, 0.05) == 0:
        print('Continuing')

    # Turn off epsilon
    agent.epsilon = 0

    # Return the average length of 500 episodes
    ep_len = np.mean([run_ep(env, agent) for _ in xrange(500)])
    return ep_len

def q3ii_inner_func((delta, run)):
    """ Just the inner function for the pool """
    return pd.DataFrame({
        'delta': [delta],
        'length': [q3ii_ep_len(delta, 15000)],
        'run': ['run%d' % run]}, dtype=np.float)

def q3ii_save_fig(out_fn, deltas, n_runs):
    """
    Gets results for running Sarsa on various delta and saves plot
    :param out_fn: str - Filename to save figure to
    :param deltas: [float] - List of values to use for delta
    :param n_runs: int - number of runs per delta
    :return: None
    """
    # Get results in parallel
    pl = multiprocessing.Pool(multiprocessing.cpu_count())
    dfs = pl.map(q3ii_inner_func, list(itertools.product(deltas, xrange(n_runs))),
                 chunksize=max(1, len(deltas) * n_runs / (4 * multiprocessing.cpu_count())))
    df = pd.concat(dfs)
    pl.close()

    df.to_pickle(out_fn.split('.')[0] + '.dat')
    # Plot this with seaborn
    sns.tsplot(df, time='delta', unit='run', value='length',
               err_style="ci_band", ci=np.linspace(95,10,4))
    plt.savefig(out_fn, dpi=600, bbox_inches='tight')
    plt.close()

    # Plot the unit traces
    sns.tsplot(df, time='delta', unit='run', value='length',
               err_style="unit_traces")
    plt.savefig(out_fn.split('.')[0] + '_unit.pdf', dpi=600, bbox_inches='tight')
    plt.close()

def q4_ep_len(agent_class, epsilon, eps_off, max_episodes):
    """
    Function that finds the average final episode length for a epsilon as in Q4
    :param agent_class: RLAgent class
    :param eps_off: Bool - turn epsilon off for last 500
    :param epsilon: float - wind probability
    :param max_episodes: int - maximum time to convergence
    :return: average final episode length
    """
    print('Getting result with epsilon=%.2f' % epsilon)
    # Set up environment and agent
    env = get_windy_grid_water(0, -100)
    agent = agent_class(0.1, 0.99, epsilon, env.num_states, env.actions)

    # Converge agent
    if converge(env, agent, max_episodes, 0.05) == 0:
        print('Continuing')

    # Turn off epsilon
    if eps_off:
        agent.epsilon = 0

    # Return the average length of 500 episodes
    ep_len = np.mean([run_ep(env, agent) for _ in xrange(500)])
    return ep_len

def q4_inner_func((agent, epsilon, eps_off, run)):
    """ Just the inner function for the pool """
    return pd.DataFrame({
        'epsilon': [epsilon],
        'epsilon off': [eps_off],
        'agent': [str(agent)],
        'length': [q4_ep_len(agent, epsilon, eps_off, 15000)],
        'run': ['run%d' % run]}, dtype=np.float)

def q4_save_fig(out_fn, epsilons, n_runs):
    """
    Gets results for running Sarsa and QLearn on various delta and saves plot
    :param out_fn: str - Filename to save figure to
    :param epsilons: [float] - List of values to use for epsilon
    :param n_runs: int - number of runs per delta
    :return: None
    """
    # Parameters
    agents = (QLearn, Sarsa)
    eps_offs = (True, False)

    # Get results in parallel
    pl = multiprocessing.Pool(multiprocessing.cpu_count())
    dfs = pl.map(q4_inner_func, list(itertools.product(agents, epsilons, eps_offs, xrange(n_runs))),
                 chunksize=max(1, len(epsilons) * n_runs / (4 * multiprocessing.cpu_count())))
    df = pd.concat(dfs)
    pl.close()

    df.to_pickle(out_fn.split('.')[0] + '.dat')

    # Plot Epslilon off
    sns.tsplot(data=df.loc[df['epsilon off']==True], time='epsilon', unit='run', value='length',
               condition='agent', err_style="ci_band", ci=np.linspace(95, 10, 4))
    plt.savefig(out_fn.split('.')[0] + '_epsoff.pdf', dpi=600, bbox_inches='tight')
    plt.close()

    sns.tsplot(data=df.loc[df['epsilon off']==True], time='epsilon', unit='run', value='length',
               condition='agent', err_style="unit_traces", n_boot=0)
    plt.savefig(out_fn.split('.')[0] + '_epsoff_unit.pdf', dpi=600, bbox_inches='tight')
    plt.close()

    # Plot Epsilon on
    sns.tsplot(data=df.loc[df['epsilon off']==False], time='epsilon', unit='run', value='length',
               condition='agent', err_style="ci_band", ci=np.linspace(95, 10, 4))
    plt.savefig(out_fn.split('.')[0] + '_epson.pdf', dpi=600, bbox_inches='tight')
    plt.close()

    sns.tsplot(data=df.loc[df['epsilon off']==False], time='epsilon', unit='run', value='length',
               condition='agent', err_style="unit_traces", n_boot=0)
    plt.savefig(out_fn.split('.')[0] + '_epson_unit.pdf', dpi=600, bbox_inches='tight')
    plt.close()

def q4_reward_inner_func((agent_class, epsilon, eps_off, run)):
    """ Just the inner function for the pool """
    print('Getting result with epsilon=%.2f' % epsilon)
    # Set up environment and agent
    env = get_windy_grid_water(0, -100)
    agent = agent_class(0.1, 0.99, epsilon, env.num_states, env.actions)

    if converge(env, agent, 15000, 0.05) == 0:
        print('Continuing')

    # Turn off epsilon
    if eps_off:
        agent.epsilon = 0

    # the average reward of 500 episodes
    reward = np.mean([run_ep(env, agent, reward=True) for _ in xrange(500)])

    return pd.DataFrame({
        'epsilon': [epsilon],
        'epsilon off': [eps_off],
        'agent': [str(agent_class)],
        'reward': [reward],
        'run': ['run%d' % run]}, dtype=np.float)


def q4_reward_save_fig(out_fn, epsilons, n_runs):
    """
    Gets results for running Sarsa and QLearn on various delta and saves plot
    :param out_fn: str - Filename to save figure to
    :param epsilons: [float] - List of values to use for epsilon
    :param n_runs: int - number of runs per delta
    :return: None
    """
    # Parameters
    agents = (QLearn, Sarsa)
    eps_offs = (True, False)

    # Get results in parallel
    pl = multiprocessing.Pool(multiprocessing.cpu_count())
    dfs = pl.map(q4_reward_inner_func, list(itertools.product(agents, epsilons, eps_offs, xrange(n_runs))),
                 chunksize=max(1, len(epsilons) * n_runs / (4 * multiprocessing.cpu_count())))
    df = pd.concat(dfs)
    pl.close()

    df.to_pickle(out_fn.split('.')[0] + '.dat')

    # Plot Epslilon off
    sns.tsplot(data=df.loc[df['epsilon off']==True], time='epsilon', unit='run', value='reward',
               condition='agent', err_style="ci_band", ci=np.linspace(95, 10, 4))
    plt.savefig(out_fn.split('.')[0] + '_epsoff.pdf', dpi=600, bbox_inches='tight')
    plt.close()

    sns.tsplot(data=df.loc[df['epsilon off']==True], time='epsilon', unit='run', value='reward',
               condition='agent', err_style="unit_traces", n_boot=0)
    plt.savefig(out_fn.split('.')[0] + '_epsoff_unit.pdf', dpi=600, bbox_inches='tight')
    plt.close()

    # Plot Epsilon on
    sns.tsplot(data=df.loc[df['epsilon off']==False], time='epsilon', unit='run', value='reward',
               condition='agent', err_style="ci_band", ci=np.linspace(95, 10, 4))
    plt.savefig(out_fn.split('.')[0] + '_epson.pdf', dpi=600, bbox_inches='tight')
    plt.close()

    sns.tsplot(data=df.loc[df['epsilon off']==False], time='epsilon', unit='run', value='reward',
               condition='agent', err_style="unit_traces", n_boot=0)
    plt.savefig(out_fn.split('.')[0] + '_epson_unit.pdf', dpi=600, bbox_inches='tight')
    plt.close()

def q4i_get_result_eps(epsilon, agent_class, max_time):
    """
    Function that returns the number of episodes completed by QLearn before max_time
    :param epsilon: float - Epsilon for Sarsa
    :param agent_class: RLAgent class
    :param max_time: int - Maximum number of timesteps for each alpha
    :return: [int] - Cumulative timesteps where episodes finished
    """
    # Set up environment and agent
    env = get_windy_grid_water(0, -100)
    agent = agent_class(0.1, 0.99, epsilon, env.num_states, env.actions)

    # Loop until max_time has been reached
    steps = [0]
    while steps[-1] < max_time:
        # Append trace for episode
        steps.append(steps[-1] + run_ep(env, agent, max_time))

    # Get rid of last data point if past max_time
    if steps[-1] > max_time:
        steps.pop()

    # Return the cumulative timesteps where episodes finished
    return steps

def q4i_inner_func((max_time, agent, epsilon, run)):
    """ Just the inner function for the pool """
    steps = q4i_get_result_eps(epsilon, agent, max_time)

    # Return the column of the DataFrame with episode number as result
    return pd.DataFrame(range(len(steps)), index=steps,
                        columns=pd.MultiIndex.from_tuples([(str(agent), epsilon, run)],
                                                          names=['agent', 'epsilon', 'run']))

def q4i_save_fig(out_fn, epsilons, max_time, n_runs):
    """
    Gets results for running Sarsa on various alpha and saves plot
    :param out_fn: str - Filename to save figure to
    :param epsilons: [float] - List of values to use for epsilon
    :param max_time: int - Maximum number of timesteps for each alpha
    :param n_runs: int - number of runs per alpha
    :return: None
    """
    # Parameters
    agents = (Sarsa, QLearn)
    # Get results in parallel
    pl = multiprocessing.Pool(multiprocessing.cpu_count())
    dfs = pl.map(q4i_inner_func, itertools.product([max_time], agents, epsilons, xrange(n_runs)))
    pl.close()

    # Concatenate results sideways
    df = pd.concat(dfs, axis=1)

    # Interpolate missing values so that there is meaningful data for all timesteps
    df.interpolate(method='values', inplace=True)

    # Save data
    df.to_pickle(out_fn.split('.')[0] + '.dat')

    # Get data into long-form for plotting
    ldf = df.unstack().reset_index()
    ldf.columns = ['agent', 'epsilon', 'run', 'time', 'episode']

    # Plot this with seaborn
    sns.tsplot(ldf.loc[ldf['agent']==str(Sarsa)], time='time', unit='run', condition='epsilon', value='episode',
               err_style="ci_band", ci=99, n_boot=1500)
    plt.savefig(out_fn.split('.')[0] + '_Sarsa.pdf', dpi=600, bbox_inches='tight')
    plt.close()

    sns.tsplot(ldf.loc[ldf['agent']==str(Sarsa)], time='time', unit='run', condition='epsilon', value='episode',
               err_style="unit_traces", n_boot=0)
    plt.savefig(out_fn.split('.')[0] + '_Sarsa_unit.pdf', dpi=600, bbox_inches='tight')
    plt.close()

    sns.tsplot(ldf.loc[ldf['agent']==str(QLearn)], time='time', unit='run', condition='epsilon', value='episode',
               err_style="ci_band", ci=99, n_boot=1500)
    plt.savefig(out_fn.split('.')[0] + '_QLearn.pdf', dpi=600, bbox_inches='tight')
    plt.close()

    sns.tsplot(ldf.loc[ldf['agent']==str(QLearn)], time='time', unit='run', condition='epsilon', value='episode',
               err_style="unit_traces", n_boot=0)
    plt.savefig(out_fn.split('.')[0] + '_QLearn_unit.pdf', dpi=600, bbox_inches='tight')
    plt.close()

if __name__ == '__main__':
    main()
