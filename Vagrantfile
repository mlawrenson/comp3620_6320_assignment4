# -*- mode: ruby -*-
# vi: set ft=ruby :

Vagrant.configure("2") do |config|
  # Resources to give the VM - feel free to modify
  RAM = 4096
  CPUS = 8

  config.vm.hostname = "comp3620-a4"

  config.vm.post_up_message = "The files inside this folder are mapped to /vagrant."

  config.vm.box = "hashicorp/precise64"

  config.vm.provider "hyperv" do |v|
    v.memory = RAM
    v.cpus = CPUS
  end

  config.vm.provider "vmware_workstation" do |v|
    v.vmx['memsize'] = RAM
    v.vmx['numvcpus'] = CPUS
  end

  config.vm.provider "vmware_fusion" do |v|
    v.vmx['memsize'] = RAM
    v.vmx['numvcpus'] = CPUS
  end

  config.vm.provider "virtualbox" do |v|
    v.memory = RAM
    v.cpus = CPUS
  end

  config.ssh.forward_x11 = true

  config.vm.provision "shell", inline: <<-SCRIPT
  apt-get update
  apt-get -y install ca-certificates
  echo 'export PATH=/opt/conda/bin:$PATH' > /etc/profile.d/conda.sh
  wget --quiet https://repo.continuum.io/archive/Anaconda-2.2.0-Linux-x86_64.sh
  /bin/bash Anaconda-2.2.0-Linux-x86_64.sh -b -p /opt/conda
  rm Anaconda-2.2.0-Linux-x86_64.sh
  /opt/conda/bin/conda install --yes conda
  /opt/conda/bin/conda install --yes seaborn
  export PATH=/opt/conda/bin:$PATH
  SCRIPT
end