The dependencies for experiments.py are:
    numpy
    pandas
    matplotlib
    seaborn

If you are using Anaconda/Miniconda, you can just run:
    conda install seaborn

Conda will sort out all dependencies.


If you don't have conda but have pip, then running:
    pip install pandas
    pip install matplotlib
    pip install seaborn

Should work fine. You may need to install compilers depending on your OS.

If you do not have pip (it is included from 2.7.9) you can get it from here:
    https://pip.pypa.io/en/stable/installing.html


You can also just use Vagrant, which is pretty great.
  Firstly, you need VirtualBox or Hyper-V. Get VirtualBox from:
    https://www.virtualbox.org/wiki/Downloads
    (If you are on Windows 8.1 you should already have Hyper-V)

  Then, get Vagrant from:
    https://www.vagrantup.com/

  You may want to edit the resources given to the guest, these are near the top of
  the Vagrantfile. 

  Now just run
    vagrant up
  In this folder.

  Vagrant will automatically download and configure Ubuntu, Anaconda and Seaborn.

  To access it, if you are on Linux, just run:
    vagrant ssh

  If you are on Mac, you will need XQuartz, which you can get from here:
    http://xquartz.macosforge.org/landing/
    Then run:
    vagrant ssh

  On Windows you will need OpenSSH and Cygwin/X, both are included in Cygwin/X:
     http://x.cygwin.com/docs/ug/setup.html#setup-cygwin-x-installing
     Then, run:
     bash startxwin
     vagrant ssh (From the X Terminal)

  If you don't want to bother with X11 forwarding (I don't acutally display any plots)
  just uncomment the two lines near the top of experiments.py.

  The files in this folder will be mapped to /vagrant on the VM.


I have tested everything with Vagrant with Hyper-V Server and Virtualbox.

Feel free to contact me if you have any issues at:
mark@lawrenson.id.au